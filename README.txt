Entity Backup
--------------
Main purpose of this module is to keep the backup of deleted drupal code 
entities and perform recovery of the backuped entities.

Installation
--------------
Download module
Upload module to sites/all/modules
Enable module
Flush All of Drupal's Caches

Permissions
------------
To make changes to the Entity backup settings, users need the "administer 
entity backup" permission.
User have administer entity backup will able to see and recover deleted entities

Access
-------
You can restore from backup from admin/content/entity_backup_form.
There are two options in actions
1: delete
2: restore
  Delete: This action delete the created backup by this module.
  Restore: This action restore the entity and Delete the backup too.

Settings
----------
There is delete and restore options on admin/content/entity_backup_form menu
with confirmation box. If you want to disable or enable please go to url

admin/config/content/entity_backup_form

for confirmation box setting.

Limitations
------------
Supports Drupal's Core Entity Types for creating the backup.

    1)Comments
    2)Files
    3)Nodes
    4)Taxonomy Terms
    5)Taxonomy Vocabularies
    6)Users

    NOTE:
    ---------- It restores except Files Entity-------------------------

    It restore the user with Its UID.
    It restore comment with different Cid.
    It restore the nodes as new node with new Nid.
    It restore taxonomy_vocabulary with new Vid.
    It restore taxonomy_term with new TID.

Author
------
Rajveer Singh

*rajveer.gang@gmail.com
*https://www.drupal.org/u/rajveergang
