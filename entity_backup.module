<?php

/**
 * @file
 * Main file for Entity Backup module.
 */

/**
 * Implements hook_help().
 */
function entity_backup_help($path, $arg) {
  switch ($path) {
    // Main module help for the cps module.
    case 'admin/help#entiy_backup':
      $output = t('This module keeps backup of deleted entity, So user can restore it again.');
      $output .= t('This module provides backup facility only for drupal core entities. Except file entity.');
      return $output;
  }
}

/**
 * Implements hook_entity_delete().
 */
function entity_backup_entity_delete($entity, $type) {
  // IF code enitty then create baclup of it.
  if (entity_backup_valid_backup($type)) {
    try {
      $backup_query = db_insert('entity_backup');
      $backup_query->fields(
          array(
            'bundle' => $type,
            'title' => entity_backup_get_title($entity, $type),
            'entity_raw' => serialize($entity),
            'created' => REQUEST_TIME,
          )
      );
      $backup_query->execute();
    }
    catch (Exception $ex) {
      drupal_set_message($ex->getMessage(), 'error');
    }
  }
}

/**
 * Function used to get appropriate title of entity.
 *
 * @param object $entity
 *   Object of entity.
 * @param string $type
 *   Entity type.
 *
 * @return string
 *   String for title which is used for table display.
 */
function entity_backup_get_title($entity, $type) {
  if (entity_backup_valid_backup($type)) {
    if ($type == 'comment') {
      return $entity->subject;
    }
    elseif ($type == "node") {
      return $entity->title;
    }
    elseif ($type == 'taxonomy_term') {
      return $entity->name;
    }
    elseif ($type == 'taxonomy_vocabulary') {
      return $entity->name;
    }
    elseif ($type == 'user') {
      return $entity->name;
    }
    else {
      return $entity->filename;
    }
  }
}

/**
 * Function which gives valid entity type flag.
 *
 * @param mixed $entity_type
 *   Entity type.
 *
 * @return bool
 *   Bool value which entity is supported by this module.
 */
function entity_backup_valid_backup($entity_type) {
  $valid_entity = array(
    'comment',
    'node',
    'taxonomy_term',
    'taxonomy_vocabulary',
    'user',
    'file',
  );
  if (in_array($entity_type, $valid_entity)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_permission().
 */
function entity_backup_permission() {
  return array(
    'administer entity backup' => array(
      'title' => t('administer entity backup'),
      'description' => t('administer administer entity backup permission'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function entity_backup_menu() {
  $items = array();

  $items['admin/content/entity_backup_form'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Entity Backup',
    'description' => 'Entity Backup',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_backup_form'),
    'access arguments' => array('administer entity backup'),
  );

  $items['admin/content/entity_backup_form/delete/%'] = array(
    'type' => MENU_CALLBACK,
    'page arguments' => array(4),
    'page callback' => 'entity_backup_delete',
    'access arguments' => array('administer entity backup'),
  );

  $items['admin/content/entity_backup_form/restore/%'] = array(
    'type' => MENU_CALLBACK,
    'page arguments' => array(4),
    'page callback' => 'entity_backup_restore',
    'access arguments' => array('administer entity backup'),
  );

  $items['admin/config/content/entity_backup_form'] = array(
    'title' => 'Entity Backup Setting',
    'description' => 'Entity Backup Setting For Confirmation Box.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_backup_setting_form'),
    'access arguments' => array('administer entity backup'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_form().
 */
function entity_backup_setting_form() {
  $form = array();
  $form['entity_backup_js_setting'] = array(
    '#type' => 'radios',
    '#title' => t('Confirmation box active status'),
    '#options' => array("No", "Yes"),
    '#default_value' => variable_get('entity_backup_js_setting', 1),
  );
  return system_settings_form($form);
}

/**
 * Function which is used to delete record from table.
 *
 * @param int $row_id
 *   Row id.
 * @param bool $redirect
 *   Bool for redirect.
 * @param bool $is_file
 *   Bool for file entity.
 */
function entity_backup_delete($row_id, $redirect = TRUE, $is_file = FALSE) {
  try {
    $delete_query = db_delete('entity_backup');
    $delete_query->condition('id', $row_id);
    $delete_query->execute();
    if ($is_file) {
      drupal_goto('admin/content/entity_backup_form');
    }
    if ($redirect) {
      drupal_set_message(t("Record deleted successfully"));
    }
    else {
      drupal_set_message(t("Record restored successfully"));
    }
    drupal_goto('admin/content/entity_backup_form');
  }
  catch (Exception $ex) {
    drupal_set_message($ex->getMessage);
  }
}

/**
 * Function which is used to restore entity from table.
 *
 * @param int $row_id
 *   Row id.
 */
function entity_backup_restore($row_id) {
  $row_data = unserialize(entity_backup_get_db_data($row_id));
  if (is_object($row_data)) {
    $type = $_GET['bundle'];
    switch ($type) {
      case 'user':
        user_save('', (array) $row_data);
        entity_backup_delete($row_id, FALSE);
        break;

      case 'node':
        $row_data->is_new = TRUE;
        unset($row_data->nid);
        unset($row_data->vid);
        unset($row_data->tnid);
        node_save($row_data);
        entity_backup_delete($row_id, FALSE);
        break;

      case 'comment':
        $row_data->cid = 0;
        comment_save($row_data);
        entity_backup_delete($row_id, FALSE);
        break;

      case 'taxonomy_vocabulary':
        unset($row_data->vid);
        taxonomy_vocabulary_save($row_data);
        entity_backup_delete($row_id, FALSE);
        break;

      case 'taxonomy_term':
        unset($row_data->tid);
        taxonomy_term_save($row_data);
        entity_backup_delete($row_id, FALSE);
        break;

      case 'file':
        drupal_set_message(t("File can't be restored."));
        entity_backup_delete($row_id, FALSE, TRUE);
        break;
    }
  }
}

/**
 * Function to get rows w.r.t. row id.
 *
 * @param int $row_id
 *   Row id.
 *
 * @return string
 *   String of data or row data.
 */
function entity_backup_get_db_data($row_id) {
  try {
    $query = db_select('entity_backup', 'eb');
    $query->fields('eb', array('entity_raw'));
    $query->condition('id', $row_id);
    $data = $query->execute()->fetchField();
  }
  catch (Exception $ex) {
    $data = $ex->getMessage();
  }
  return $data;
}

/**
 * Implements hook_form().
 */
function entity_backup_form($form, &$form_state) {
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Form'),
  );

  $form['fieldset']['bundle'] = array(
    '#type' => 'textfield',
    '#title' => t("Bundle"),
    '#description' => t('Please enter bundle type'),
    '#default_value' => isset($_GET['bundle']) ? $_GET['bundle'] : "",
  );

  $form['fieldset']['title'] = array(
    '#type' => 'textfield',
    '#title' => t("Title"),
    '#description' => t('Please enter title'),
    '#default_value' => isset($_GET['title']) ? $_GET['title'] : "",
  );

  $form['fieldset']['display_button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['fieldset']['reset'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Reset'), 'admin/content/entity_backup_form'),
  );

  $form['markup'] = array(
    '#type' => 'markup',
    '#markup' => entity_backup_rows(),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function entity_backup_form_submit($form, &$form_state) {

  $title = $form_state['values']['title'];
  $bundle = $form_state['values']['bundle'];

  drupal_goto('admin/content/entity_backup_form', array(
    'query' => array(
      'title' => $title,
      'bundle' => $bundle,
    ),
  ));
}

/**
 * Function to get rows table html form filter form.
 *
 * @return mixed
 *   Returns html table.
 */
function entity_backup_rows() {
  if (variable_get('entity_backup_js_setting', 1)) {
    drupal_add_js(drupal_get_path('module', 'entity_backup') . '/js/entity_backup.js', array('type' => 'file', 'scope' => 'footer'));
  }
  $html = '';
  $rows = array();

  $bundle = isset($_GET['bundle']) ? $_GET['bundle'] : '';
  $title = isset($_GET['title']) ? $_GET['title'] : '';

  $header = array(
    t('Serial Number'),
    t('Entity Title'),
    t('Bundle Type'),
    t('Created'),
    t('Operations'),
  );
  $query = db_select('entity_backup', 'eb');
  $query->fields('eb', array('id', 'title', 'bundle', 'created'));

  if (isset($bundle) && !empty($bundle)) {
    $query->condition('eb.bundle', $bundle);
  }
  if (isset($title) && !empty($title)) {
    $query->condition('eb.title', $title);
  }
  $query = $query->extend('PagerDefault')->limit(50);
  $query->orderBy('eb.id', 'DESC');

  $result = $query->execute();
  $sn = 0;
  while ($record = $result->fetchObject()) {
    $restore_link = l(t("Restore"), "admin/content/entity_backup_form/restore/" . $record->id, array("query" => array("bundle" => $record->bundle), "attributes" => array("class" => array("entity-backup-restore"))));
    $delete_link = l(t("Delete"), "admin/content/entity_backup_form/delete/" . $record->id, array("attributes" => array("class" => array("entity-backup-delete"))));
    $rows[] = array(
      ++$sn,
      $record->title,
      $record->bundle,
      format_date($record->created),
      $delete_link . " | " . $restore_link,
    );
  }
  $html .= t("Total Result: <b>@count</b>", array("@count" => count($rows)));
  $html .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No Record Found!'),
  ));
  $html .= theme('pager');

  return $html;
}
