
/**
 * @file
 * Main file for entity backup module.
 */
(function ($) {
  'use strict';
  Drupal.behaviors.entity_backup = {
    attach: function (context, settings) {
      $('.entity-backup-restore', context).click(function () {
        if (confirm('Are you sure you want to restore this content ?')) {
          return true;
        }
        else {
          return false;
        }
      });

      $('.entity-backup-delete', context).click(function () {
        if (confirm('Are you sure you want to delete this backup ?')) {
          return true;
        }
        else {
          return false;
        }
      });
    }
  };
})(jQuery);
